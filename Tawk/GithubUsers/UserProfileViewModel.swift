//
//  UserProfileViewModel.swift
//  Tawk
//
//  Created by Rahul Mahajan on 29/03/22.
//

import Foundation
import Combine
import UIKit

class UserProfileViewModel: NSObject {
    
    var userName:String
    private var cancellable = Set<AnyCancellable>()
    
    var bindUserToController: (() -> ()) = {}
    var bindImageToController: ((UIImage?) -> ()) = { _ in }
    var user:User?
    private var cancellableImage: AnyCancellable?
    
    init(name:String) {
        self.userName = name
    }
    
    func getUserProfile() {
        
        let _ = GithubAPI.userProfile(name: self.userName).sink(receiveCompletion: { _ in },
                                                                receiveValue: { value in self.user = value
            self.getImage()
            self.bindUserToController()
        }).store(in: &cancellable)
    }
    
    func getImage() {
        cancellableImage = loadImage(for: self.user?.avatar_url ?? "").sink { [unowned self] image in self.bindImageToController(image) }
    }
    
    private func loadImage(for urlString: String) -> AnyPublisher<UIImage?, Never> {
        return Just(urlString)
        .flatMap({ poster -> AnyPublisher<UIImage?, Never> in
            let url = URL(string: urlString)!
            return ImageLoader.shared.loadImage(from: url)
        })
        .eraseToAnyPublisher()
    }
    
}
