//
//  ViewController.swift
//  Tawk
//
//  Created by Rahul Mahajan on 22/03/22.
//

import UIKit
import Combine

public protocol UserListCoordinatorDelegate: class {
    func navigateToProfile()
}

class GithubUsersViewController: UIViewController, UISearchResultsUpdating {
    
    @IBOutlet weak var userTableView: UITableView! {
        didSet {
            self.userTableView.delegate = self
            self.userTableView.dataSource = self
        }
    }
    
    var coordinatorDelegate:UserListCoordinatorDelegate?
    internal var viewModel:GithubUsersViewModel!
    var searchResultController = UISearchController()
    var alert:UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = GithubUsersViewModel()
        self.registerCells()
        self.navigationController?.isNavigationBarHidden = true
        
        self.viewModel.bindUsersToController = {
            self.userTableView.reloadData()
            self.alert?.dismiss(animated: true, completion: nil)
        }
        
        searchResultController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.obscuresBackgroundDuringPresentation  = false
            controller.searchBar.sizeToFit()
            
            self.userTableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        viewModel.showLoader = {
            self.showLoader()
        }
    }
    
    fileprivate func registerCells() {
        self.userTableView.register(UINib(nibName: "UserDetailTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "UserDetailTableViewCell")
    }
    
    func showLoader() {
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert?.view.addSubview(loadingIndicator)
        
        if let vc = alert {
            present(vc, animated: true, completion: nil)
        }
    }
}

extension GithubUsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchResultController.isActive {
            return viewModel.filteredItems.count
        } else {
            return viewModel.items.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item:CellConfigurator?
        
        if searchResultController.isActive {
            item = viewModel.filteredItems[indexPath.row]
        } else {
            item = viewModel.items[indexPath.row]
        }
        
        guard let result = item else { return UITableViewCell()}
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: result).reuseId)!
        result.configure(cell: cell)
        
        if indexPath.row == viewModel.users.count - 1 {
            viewModel.loadMoreUsers()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userName = viewModel.users[indexPath.row].login ?? ""
        
        if let userProfile = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "UserProfileViewController") as? UserProfileViewController {
            userProfile.viewModel = UserProfileViewModel(name: userName)
    
            self.navigationController?.pushViewController(userProfile, animated: true)
        }
               
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.filteredItems.removeAll(keepingCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        
        let filtered = viewModel.users.filter {
            searchPredicate.evaluate(with: $0.login)
        }
        
        for user in filtered {
            viewModel.filteredItems.append( UserCellConfig.init(item: user))
        }
        
        self.userTableView.reloadData()
    }
    
}


