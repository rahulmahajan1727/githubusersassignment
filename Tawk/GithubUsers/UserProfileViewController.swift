//
//  UserProfileViewController.swift
//  Tawk
//
//  Created by Rahul Mahajan on 23/03/22.
//

import UIKit
import Combine

class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var infoView: UIView! {
        didSet {
            self.infoView.layer.cornerRadius = 10.0
            self.infoView.layer.borderWidth = 1.0
            self.infoView.layer.borderColor = UIColor.gray.cgColor
        }
    }
    @IBOutlet weak var githubProfile: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    weak var coordinatorDelegate:UserProfileCoordinatorDelegate?
    
    var viewModel:UserProfileViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        viewModel?.getUserProfile()
        
        viewModel?.bindUserToController = {
            self.name.text = self.viewModel?.user?.login
            self.userName.text = self.viewModel?.user?.login
            self.githubProfile.text = self.viewModel?.user?.login
            self.followers.text = "\(self.viewModel?.user?.followers ?? 0)"
            self.followers.text = "\(self.viewModel?.user?.following ?? 0)"
            self.githubProfile.text = self.viewModel?.user?.url
        }
        
        viewModel?.bindImageToController = { image in
            self.userImage.image = image
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

