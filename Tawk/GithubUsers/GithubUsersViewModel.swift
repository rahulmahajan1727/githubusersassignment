//
//  GithubUsersViewModel.swift
//  Tawk
//
//  Created by Rahul Mahajan on 24/03/22.
//

import Foundation
import Combine

typealias UserCellConfig = TableCellConfigurator<UserDetailTableViewCell, User>

class GithubUsersViewModel: NSObject {
    
    private (set) var users: [User] = [] {
        didSet {
            self.prepareCellModel()
            self.bindUsersToController()
        }
    }
    
    var items: [CellConfigurator] = []
    var filteredItems : [CellConfigurator] = []
    
    var bindUsersToController: (() -> ()) = {}
    var showLoader: (() -> ()) = {}
    private var cancellable = Set<AnyCancellable>()
    
    override init() {
        super.init()
        self.checkForUsers()
    }
    
    fileprivate func checkForUsers() {
        
        let githubUsers = CoreDataManager(modelName: "Tawk").retrieveUsers()
        
        if githubUsers.count == 0 {
            self.showLoader()
            let _ = GithubAPI.users(since: 0).sink(receiveCompletion: { _ in },
                                                   receiveValue: { value in self.users = value
                CoreDataManager(modelName: "Tawk").createData(for: value)

            }).store(in: &cancellable)
        } else {
            self.convertGitToUser(githubUser: githubUsers)
        }

    }
    
    func loadMoreUsers() {
        let _ = GithubAPI.users(since: self.users.count).sink(receiveCompletion: { _ in },
                                                              receiveValue: { value in self.users.append(contentsOf: value)
            CoreDataManager(modelName: "Tawk").createData(for: value)
        }).store(in: &cancellable)
    }
    
    fileprivate func prepareCellModel() {
        for user in users {
            items.append( UserCellConfig.init(item: user))
        }
    }
    
    func convertGitToUser(githubUser:[GithubUser]) {
        for eachUser in githubUser {
            let user = User()
            user.login = eachUser.login
            user.id = Int(eachUser.id)
            user.avatar_url = eachUser.avatar_url
            
            self.users.append(user)
        }
    }
    
}
