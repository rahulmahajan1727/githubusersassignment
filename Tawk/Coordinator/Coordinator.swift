//
//  Cooridnator.swift
//  Tawk
//
//  Created by Rahul Mahajan on 23/03/22.
//

import UIKit

public protocol Coordinator : class {

    var childCoordinators: [Coordinator] { get set }

    // All coordinators will be initilised with a navigation controller
    init(navigationController:UINavigationController)

    func start()
}

public protocol UserProfileCoordinatorDelegate: class {
    func navigateToUserList()
}


