//
//  AppCoordinator.swift
//  Tawk
//
//  Created by Rahul Mahajan on 23/03/22.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator, UserListCoordinatorDelegate {
    
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController:UINavigationController
    var userList:GithubUsersViewController?
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let githubUsersVc : GithubUsersViewController = GithubUsersViewController()
        githubUsersVc.coordinatorDelegate = self
        self.navigationController.pushViewController(githubUsersVc, animated: true)
        self.userList = githubUsersVc
    }
    
}

extension AppCoordinator : UserProfileCoordinatorDelegate {
        
    // Navigate to Users List
    func navigateToUserList() {
        navigationController.popToRootViewController(animated: true)
        childCoordinators.removeLast()
    }
    
    // Navigate to Profile
    func navigateToProfile() {
        let userProfileVc : UserProfileViewController = UserProfileViewController()
        userProfileVc.coordinatorDelegate = self
        self.navigationController.pushViewController(userProfileVc, animated: true)
    }
}
