//
//  UserDetailTableViewCell.swift
//  Tawk
//
//  Created by Rahul Mahajan on 24/03/22.
//

import UIKit
import Combine

class UserDetailTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var userDetailsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var avatarView: UIImageView! {
        didSet {
            self.avatarView.layer.cornerRadius = 20.0
        }
    }
    
    private var cancellable: AnyCancellable?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(data user: User) {
        userNameLabel.text = user.login
        cancellable = loadImage(for: user.avatar_url ?? "").sink { [unowned self] image in self.avatarView.image = image }
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        self.avatarView.image = nil
        self.userNameLabel.text = ""
        cancellable?.cancel()
    }
    
    private func loadImage(for urlString: String) -> AnyPublisher<UIImage?, Never> {
        return Just(urlString)
        .flatMap({ poster -> AnyPublisher<UIImage?, Never> in
            let url = URL(string: urlString)!
            return ImageLoader.shared.loadImage(from: url)
        })
        .eraseToAnyPublisher()
    }
    
}
