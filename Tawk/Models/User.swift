//
//  User.swift
//  Tawk
//
//  Created by Rahul Mahajan on 22/03/22.
//

import Foundation

class User:Codable, Equatable, Hashable {
    var login:String?
    var id:Int?
    var avatar_url: String?
    var followers: Int?
    var following:Int?
    var url:String?
    
    var hashValue: Int { get { return id ?? 0 } }
    
    init() {}
    
    static func ==(left:User, right:User) -> Bool {
        return left.id == right.id
    }
}


