//
//  Github.swift
//  Tawk
//
//  Created by Rahul Mahajan on 22/03/22.
//

import Foundation
import Combine

enum GithubAPI {
    static let agent = Agent()
    static let base = URL(string: "https://api.github.com/")!
}

extension GithubAPI {
    
    static func users(since:Int) -> AnyPublisher<[User], Error> {
        let usersURL = base.appendingPathComponent("users")
        let sinceString = String(since)
        let finalURL = usersURL.appending("since", value: sinceString)
        return run(URLRequest(url:finalURL ))
    }
    
    static func userProfile(name:String) -> AnyPublisher<User,Error> {
        let profileURL = base.appendingPathComponent("users/\(name)")
        return run(URLRequest(url: profileURL))
    }
    
    static func run<T:Decodable>(_ request:URLRequest) -> AnyPublisher<T, Error> {
        return agent.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }
}
